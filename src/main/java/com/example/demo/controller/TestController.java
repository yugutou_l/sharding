package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yugutou
 * @create 2021/4/6
 */
@RestController
public class TestController {

    @GetMapping("/test")
    public String test(){
        return "helloWord2";
    }
}